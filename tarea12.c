#include <stdio.h>
int main(int argc, char const *argv[]) {
  /* los empleados por comision reciben su pago segun
  el valor de los bienes y servicios que venden.

Una empresa de ventas le paga a sus trabajadores sueldos por comisión de la siguiente manera:
Si venden de 1 a 12 piezas le pagan el 5% del total de las ventas.
Si venden de 13 a 30 piezas le pagan el 7% del total de las ventas.
Finalmente si venden mas de 30 piezas les dan el 10%.
PRECIO POR PIEZA ES DE 450.00 PESOS por menudeo (menos de 24 piezas)
y el precio de mayoreo es de 435.00*/


  float ventas, menudeo= 450.0,mayoreo=435.0;

  printf("ingrese el numero de piezas que vendio:");
  scanf("%f",&ventas);


  if (ventas<=12 && ventas!=0.0) {
    ventas*=menudeo*0.5;
    printf("su comision es:%.2f\n",ventas);
  }
  else if(ventas>=13 && ventas<=24){
    ventas*=menudeo*0.7;
    printf("su comision es:%.2f\n",ventas);
  }
  else if(ventas>24 && ventas<=30){
    ventas*=mayoreo*0.7;
    printf("su comision es:%.2f\n",ventas );
  }
  else if(ventas>30){
    ventas*=mayoreo*0.10;
    printf("su comision es:%.2f\n",ventas);
  }
  else{
    printf("usted no esta haciendo su trabajo\n");
  }
  return 0;
}
